const { Pool } = require('pg');
require('dotenv').config()
const pool = new Pool({
  connectionString: process.env.DATABASE_URL
})
const utiles = require('./utiles')



const creaGasto = async (request, response) => {
  const { fecha, tipo, importe, iva, total, proveedor, producto } = request.body

  try {
    const client = await pool.connect();
    const date = await pool.query('INSERT INTO esquema_conta.gastos (fecha, tipo, importe, iva, total, proveedor, producto) VALUES ($1, $2, $3, $4, $5, $6, $7)',
      [fecha, tipo, importe, iva, total, proveedor, producto]);

    client.release();
    response.json({ success: true })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}
const gastoMesActual = (request, response) => {
  const dia2 = new Date();
  const cadena = new Date(dia2.getFullYear(), dia2.getMonth(), 1);
  const dia1 = cadena

  pool.query(`SELECT * FROM esquema_conta.gastos WHERE fecha between $1 and $2`, [dia1, dia2], (error, results) => {
    if (error) {
      throw error
    }
    total = results.rows

    const tot = []
    for (apunte of total) {
      tot.push(parseFloat(apunte.total))
    }
       const consumible = [];
    const noConsumible = [];

    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'CONSUMIBLE')
        consumible.push(parseFloat(total[i].total))
    }
    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'NO CONSUMIBLE')
        noConsumible.push(parseFloat(total[i].total))
    }
    response.status(200).json([utiles.sum(tot).toFixed(2), utiles.sum(consumible), utiles.sum(noConsumible)]);
   
  })
  
}
const totalgastos = async (request, response) => {

  pool.query('SELECT * FROM esquema_conta.gastos ORDER BY tipo', (error, results) => {
    if (error) {
      throw error
    }
    const tipo = results.rows

    response.status(200).json(tipo)
  })

}





module.exports = {

  creaGasto,
  gastoMesActual,
  totalgastos,


}