const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const db = require('./controller')
const dbg = require('./controlGastos')

const app = express();



app.use(cors({
  origin: '*',
  optionSuccessStatus: 200,

}))

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());
app.set('trust proxy', true);




var port = process.env.PORT || 8080;

app.post('/ingresos', db.createdate)
app.get('/ingresos', db.consultaTotal)
app.get('/ingresos/mesActual',db.mesActual)
app.get('/ingresos/tipo',db.getIngresos)
app.get('/consultas/:tipo',db.getTipo1)
app.get('/rango/',db.totalApuntes)


app.post('/gasto',dbg.creaGasto)
app.get('/gastos/gastosMes',dbg.gastoMesActual)
app.get('/gastos',dbg.totalgastos)

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})
