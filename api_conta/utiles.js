const sum = (array) => {
    var acum = 0;
    for (var i = 0; i < array.length; i++) {
        acum = acum + (array[i]);
    }
    return acum;
}


module.exports = {
    sum,
}