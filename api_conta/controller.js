const { Pool } = require('pg');
require('dotenv').config()
const pool = new Pool({
  connectionString: process.env.DATABASE_URL
})
const utiles = require('./utiles')


const createdate = async (request, response) => {
  const { fecha, tipo, importe, iva, tot } = request.body

  try {
    const client = await pool.connect();
    const date = await pool.query('INSERT INTO esquema_conta.ingresos (fecha, tipo, importe, iva, total) VALUES ($1, $2, $3, $4, $5)',
      [fecha, tipo, importe, iva, tot]);

    client.release();
    response.json({ success: true })
  } catch (err) {
    console.warn('Error:', err)
    setMensaje(true)
  }
}



const consultaTotal = async (request, response) => {
  pool.query('SELECT * FROM esquema_conta.ingresos ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }

    total = results.rows
    const tot = []
    for (apunte of total) {
      tot.push(parseInt(apunte.total))
    }
    const imp = []
    for (apunte of total) {
      imp.push(parseInt(apunte.importe))
    }
    response.status(200).json([utiles.sum(tot), utiles.sum(imp)]);
  })
}

const mesActual = (request, response) => {
  const dia2 = new Date();
  const cadena = new Date(dia2.getFullYear(), dia2.getMonth(), 1);
  const dia1 = cadena

  pool.query(`SELECT * FROM esquema_conta.ingresos WHERE fecha between $1 and $2`, [dia1, dia2], (error, results) => {
    if (error) {
      throw error
    }
    total = results.rows

    const tot = []
    for (apunte of total) {
      tot.push(parseInt(apunte.total))
    }
    const imp = []
    for (apunte of total) {
      imp.push(parseInt(apunte.importe))
    }
    const tipos = [];
    for (tipo of total) {
      if (tipos.indexOf(tipo.tipo) === -1)
        tipos.push(tipo.tipo)
    }

    const CAIXA = [];
    const CAIXA2 = [];
    const TABACO = [];
    const DARDOS = [];
    const MAFARI = [];
    const KIROLBET = [];
    const OTROS = [];

    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'CAIXA')
        CAIXA.push(parseInt(total[i].total))
    }
    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'CAIXA2')
        CAIXA2.push(parseInt(total[i].total))
    }
    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'TABACO')
        TABACO.push(parseInt(total[i].total))
    }
    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'MAFARI')
        MAFARI.push(parseInt(total[i].total))
    }
    for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'KIROLBET')
        KIROLBET.push(parseInt(total[i].total))

      for (i = 0; i < total.length; i++) {
        if (total[i].tipo === 'DARDOS')
          DARDOS.push(parseInt(total[i].total))
      }

    } for (i = 0; i < total.length; i++) {
      if (total[i].tipo === 'OTROS')
        OTROS.push(parseInt(total[i].total))
    }

    response.status(200).json([utiles.sum(tot), utiles.sum(imp), utiles.sum(CAIXA), utiles.sum(CAIXA2),
    utiles.sum(TABACO), utiles.sum(MAFARI), utiles.sum(KIROLBET), utiles.sum(DARDOS), utiles.sum(OTROS),]);
  })

}
const getTipo1 = (request, response) => {

  pool.query('SELECT * FROM esquema_conta.ingresos WHERE tipo = $1', [request.params.tipo], (error, results) => {
    if (error) {
      throw error
    }

    total = results.rows
    const tot = []

    for (apunte of total) {
      tot.push(parseInt(apunte.total))
    }

    const imp = []
    for (apunte of total) {
      imp.push(parseInt(apunte.importe))
    }
    response.status(200).json([utiles.sum(tot), utiles.sum(imp)]);
  })
}


const getIngresos = async (request, response) => {

  pool.query('SELECT tipo FROM esquema_conta.ingresos GROUP BY tipo', (error, results) => {
    if (error) {
      throw error
    }
    const tipo = results.rows
    const tipos = []
    for (i in tipo) {
      tipos.push(tipo[i].tipo)
    }
    response.status(200).json(tipos)
  })

}
const totalApuntes = async (request, response) => {

  pool.query('SELECT * FROM esquema_conta.ingresos ORDER BY tipo', (error, results) => {
    if (error) {
      throw error
    }
    const tipo = results.rows

    response.status(200).json(tipo)
  })

}



module.exports = {

  createdate,
  consultaTotal,
  mesActual,
  getTipo1,
  getIngresos,
  totalApuntes,

}