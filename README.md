# CONTA

app web para la gestión de negocio de hosteleria propio.

La creación de mi primer proyecto personal desde el principio, basándome en mi negocio, con datos reales,
en la cual pueda añadir apuntes de ingresos y gastos en cualquier momento y esté donde esté, y así 
poder llevar un mayor control sobre estos, con posibilidad de 
hacer consultas a la base de datos para contrastar y obtener informes de dichos datos.

Uso para ello:
**javascript**, en la parte de backend con node.js
**React**, en frontend.
y para la base de datos uso **Posgrest SQL**.

