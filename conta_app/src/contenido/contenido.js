import React from 'react';
import { Switch, Route } from 'react-router-dom'
import './contenido.css';
import Ingresos from './ingresos';
import Gastos from './gastos';
import Home from './Home';
import Apunte from './ingresos/apunte';
import Total from './ingresos/consultas/total';
import Tipo from './ingresos/consultas/tipoapunte'
import ConsultaFechas from './ingresos/consultas/consultaFecha';
import Consultas from './ingresos/consultas/consultas';
import Gasto from './gastos/gasto';
import ConsultaGasto from './gastos/consultaGastos';
import GastosFechas from './gastos/consultaGastos/gastosFechas';





function Contenido() {
    return (
        <div >           
            <main className='body'>
                <Switch>
                <Route path="/"exact>
                        <Home/>
                    </Route>
                    <Route path="/ingresos" exact>
                        <Ingresos />
                    </Route>
                    <Route path='/ingresos/apunte' exact>
                        <Apunte />
                    </Route>
                    <Route path='/ingresos/total' exact>
                        <Total />
                    </Route>
                    <Route path='/ingresos/tipo' exact>
                        <Tipo />
                    </Route>
                    <Route path='/ingresos/consultas' exact>
                        <Consultas />
                    </Route>                    
                    <Route path='/rango/:tipo/:fechaIni/:fechaFin' exact>
                        < ConsultaFechas/>
                    </Route>
                    <Route path="/gastos" exact>
                        <Gastos/>
                    </Route>
                    <Route path="/gastos/gasto" exact>
                        <Gasto/>
                    </Route>
                    <Route path="/gastos/consulta" exact>
                        <ConsultaGasto/>
                    </Route>
                    <Route path="/gastos/consultas/:fechaIni/:fechaFin" exact>
                        <GastosFechas/>
                    </Route>
                </Switch>
            </main>
         
        </div>
    )
}

export default Contenido

