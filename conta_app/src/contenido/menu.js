import React from 'react'
import { NavLink } from 'react-router-dom'
import './menu.css'

const Menu = () => {
  return (
    <aside className='asideMenu'>
  
      <div className='ulMenu'>
        <p>
          <NavLink to="/" exact activeClassName="active">
            Home
          </NavLink>
        </p>
        <p>
          <NavLink to="/ingresos" activeClassName="active">
            ingresos
          </NavLink>
        </p>
        <p>
          <NavLink to="/gastos" activeClassName="active">
            gastos
          </NavLink>
          </p>
         
      </div>
    </aside>
  )
}

export default Menu
