import React from 'react';
import Header from '../header/header'
import Menu from './menu'



function Home(){
    return(
        <div >
            <Header/>
            <Menu/>
        </div>
    )
}
export default Home