import React from 'react';
import Menu from './menu';
import { Link } from 'react-router-dom'
import './menu.css'
import DesplegConsulta from './Utiles/DesplegConsulta';
import GastoMesActual from './gastos/gastoMesActual';




function Gastos() {
    return (
        <div className='divcont'>
            <Menu />
            <div >
                <span id='apunte'>
                    <Link to='/gastos/gasto' className='creaApunte'>
                        APUNTE GASTO
          </Link></span>
                <div className='mes'>
                    <GastoMesActual />
                </div>
                <div >
                    <DesplegConsulta>
                        <span id='apunte'>
                            <Link to='/gastos/consulta' className='creaApunte'>
                                POR RANGO DE FECHA
                        </Link></span>
                    </DesplegConsulta>
                </div>
            </div>
        </div>
    )
}
export default Gastos