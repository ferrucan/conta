import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Menu from '../../menu';


const GastosFechas = () =>{
    const { fechaIni, fechaFin } = useParams()
    const [total, setTotal] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/gastos')
            .then(r => r.json())
            .then(data => setTotal(data))
    }, [])

    if (!total) return 'loading...'
    console.log(total)
    console.log(fechaIni)
    const entries1 = total
        .filter(i => i.fecha && i.fecha.substring(0, 10) >= fechaIni)
    const entries = entries1
        .filter(j => j.fecha && j.fecha.substring(0, 10) <= fechaFin)
    const tots = entries.map(t => parseInt(t.total))
    const sum = (array) => {
        var acum = 0;
        for (var i = 0; i < array.length; i++) {
            acum = acum + (array[i]);
        }
        return acum;
    }
    const totales = sum(tots)

    if (!entries.length) return 'no existe ninguna oferta en esta fecha'
    return (
        <div className='divcont'>
            <Menu />
            <div className='rConsulta'>
                <div className='rConsultaTotal'>
                    <span>TOTAL gastos = {totales}€</span>
                    <span> desde {fechaIni} a {fechaFin}</span>
                    </div>
                <div >
                    {entries.map(apunte =>

                        <div key={apunte.id} className='unidadConsulta'>
                            <div >
                                <h4>{apunte.id}</h4>
                            </div>
                            <div >
                                <p >{apunte.fecha.substring(0, 10)}</p>
                            </div>
                            <div >
                                <h4>{apunte.producto}</h4>
                            </div>
                            <div >
                                <h4>{apunte.total}€</h4>
                            </div>

                        </div>

                    )}
                </div></div>

        </div>
    )


} 


export default GastosFechas