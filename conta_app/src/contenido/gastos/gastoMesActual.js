import React, { useState, useEffect } from 'react';

const GastoMesActual = () => {
    const [gastoMes, setGastoMes] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/gastos/gastosMes')
            .then(r => r.json())
            .then(data => setGastoMes(data))

    }, [])


    if (!gastoMes) return 'loading...'
    const diaActual = new Date()
    return (
        <div className='mesEnCurso'>
            <div className='b'>gastos mes actual &nbsp;&nbsp;
                {diaActual.getDate()}-{diaActual.getMonth() + 1}-{diaActual.getFullYear()}</div>
            <div className='c'><span>TOTAL</span>  <span >{gastoMes[0]}€&nbsp;</span></div>
            <div className='c'><span>CONSUMIBLES</span>  <span >{gastoMes[1]}€&nbsp;</span></div>
            <div className='c'><span>NO CONSUMIBLES</span>  <span >{gastoMes[2]}€&nbsp;</span></div>

        </div>
    )
}
export default GastoMesActual