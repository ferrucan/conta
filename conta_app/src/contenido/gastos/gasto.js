import React, { useState } from 'react';
import { useHistory, NavLink } from 'react-router-dom'
import Menu from '../menu';



const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}


const Gasto = () => {
  const [fecha, setFecha] = useFormField()
  const [tipo, setTipo] = useFormField()
  const [importe, setImporte] = useFormField()
  const [iva, setIva] = useFormField()
  const [total, setTotal] = useFormField()
  const [producto, setProducto] = useFormField()
  const [proveedor, setProveedor] = useFormField()
  const history = useHistory()
  const [mensaje, setMensaje] = useState(false)
  const handleSubmit = async (e) => {
    e.preventDefault()
    const gasto = { fecha, tipo, importe, iva, total, proveedor, producto }
    setMensaje(false)
    try {
      const ret = await fetch('http://localhost:8080/gasto', {
        method: 'POST',
        body: JSON.stringify(gasto),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const data = await ret.json()
      console.log(data.success)
      history.push(`/gastos`)
      if (data.success) {
      } else {
        setMensaje(true)
      }
    } catch (err) {
      console.warn('Error:', err)
      setMensaje(true)
    }
  }
  return (
    <div className='divcont'>
      <Menu />
      <p>apunte</p>
      <div className='apunte'>
        <div>
          <form onSubmit={handleSubmit} className='formApunte'>
            <p><input name='fecha' required value={fecha} onChange={setFecha}
              type="date" placeholder="fecha" className='inputFecha'
            /></p>
            <p><input name='proveedor' value={proveedor} onChange={setProveedor}
              type='text' placeholder="proveedor" className='inputImporte'
            /></p>
            <p><input name='producto' required value={producto} onChange={setProducto}
              type='text' placeholder="producto" className='inputImporte'
            /></p>
            <p><select
              className='selecTipo'
              name="tipo"
              value={tipo}
              onChange={setTipo}>
              <option  value="" className="TextContainerRow" disabled selected hidden>tipo ingreso</option>
              <option>CONSUMIBLE</option>
              <option>NO CONSUMIBLE</option>
              <option>MIXTO</option>
            </select></p>
            <p><input name='importe' required value={importe} onChange={setImporte}
              type='number' placeholder="importe" className='inputImporte'
            /></p>
            <p><select
              className='selecTipo'
              name="iva"
              type='number'
              value={iva}
              onChange={setIva}>
              <option value="" className="TextContainerRow" disabled selected hidden>tipo iva</option>
              <option>0</option>
              <option>4</option>
              <option>10</option>
              <option>21</option>
            </select></p>

            <p><input name='total' requiredvalue={total} onChange={setTotal}
              type='text' placeholder="total" className='inputImporte'
            /></p>
            <p><input name="crear apunte" type="submit" value="crear apunte" className='inputCrear' /></p>
          </form>
        </div>

      </div>

      <NavLink to="/ingresos" exact activeClassName="active">
        {mensaje} volver
          </NavLink>
    </div>
  )
}

export default Gasto