import React from 'react';
import Menu from './menu';
import { Link } from 'react-router-dom'
import './menu.css'
import MesEnCurso from './ingresos/mesEnCurso';
import DesplegConsulta from './Utiles/DesplegConsulta';


function Ingresos() {
  return (
    <div className='divcont'>
      <Menu />
      <div className='ingresos'>
        <div className='ingr'>
          <span id='apunte'>
            <Link to='/ingresos/apunte' className='creaApunte'>
              APUNTE INGRESO
          </Link></span>
          
          <div >
                <DesplegConsulta>
                    <span id='apunte' >
                        <Link to='/ingresos/consultas' className='creaApunte'>
                            POR RANGO DE FECHA
                        </Link></span>
                </DesplegConsulta>
            </div>
        </div>
        <div className='mes'>
          <MesEnCurso />
        </div>
      </div>

    </div>
  )
}
export default Ingresos