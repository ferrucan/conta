import React, { useState } from 'react'
import '../menu.css'


function DesplegConsulta({ children }) {
  const [ isExpanded, setExpanded ] = useState(false)
  const toggle = () => setExpanded(!isExpanded)

  return (
    <div className={'acordeon ' + (isExpanded ? 'abierto' : 'cerrado')}>
      <button onClick={toggle} id='apunte'>
        {isExpanded ? 'VOLVER' : 'TIPO DE CONSULTAS'}
      </button>
      {isExpanded && <div >{children}</div>}
    </div>
  )
}

export default DesplegConsulta
