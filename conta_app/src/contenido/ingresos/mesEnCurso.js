import React, { useState, useEffect } from 'react';



const MesEnCurso = () => {
    const [rango, setRango] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/ingresos/mesActual/')
            .then(r => r.json())
            .then(data => setRango(data))
    }, [])
    if (!rango) return 'loading...'

    return (
        <div className='mesEnCurso'>
            <div className='b'>ingresos mes actual</div>
            <div className='c'><span>TOTAL</span>  <span >{rango[0]}€&nbsp;</span></div>
            <div className='c'><span > SIN IVA </span> <span >{rango[1]}€&nbsp;</span></div>
            <div className='c'> <span>totCAIXA </span> <span>{rango[2]}€&nbsp;</span></div>
            <div className='c'> <span>tot: CAIXA2</span> <span>{rango[3]}€&nbsp;</span></div>
            <div className='c'> <span>tot: TABACO</span> <span>{rango[4]}€&nbsp;</span></div>
            <div className='c'> <span>tot: MAFARI</span> <span>{rango[5]}€&nbsp;</span></div>
            <div className='c'> <span>tot: KIROLBET</span> <span>{rango[6]}€&nbsp;</span></div>
            <div className='c'> <span>tot: DARDOS</span> <span>{rango[7]}€&nbsp;</span></div>
            <div className='c'> <span>tot: OTROS</span><span>{rango[8]}€&nbsp;</span></div>
            
        </div>
    )

}


export default MesEnCurso