import React, { useState } from 'react';
import { useHistory, NavLink } from 'react-router-dom'
import Menu from '../menu';
import './apunte.css'


const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}


const Apunte = () => {
  const [fecha, setFecha] = useFormField()
  const [tipo, setTipo] = useFormField()
  const [importe, setImporte] = useFormField()
  const [iva, setIva] = useFormField()
  const [total, setTotal] = useFormField()
  const history = useHistory()
  const [mensaje, setMensaje] = useState(false)
  const handleSubmit = async (e) => {
    e.preventDefault()
    const tot = parseInt((importe / 100) * iva) + parseInt(importe)
    const apunte = { fecha, tipo, importe, iva, tot }
    setMensaje(false)
    try {
      const ret = await fetch('http://localhost:8080/ingresos/', {
        method: 'POST',
        body: JSON.stringify(apunte),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const data = await ret.json()
      
      history.push(`/ingresos`)
      if (data.success) {
      } else {
        setMensaje(true)
      }
    } catch (err) {
      console.warn('Error:', err)
      setMensaje(true)
    }
  }
  return (
    <div className='divcont'>
      <Menu />
      <p>apunte</p>
      <div className='apunte'>
        <div className='apunteTitulos'>
          <p>fecha</p>
          <p>tipo apunte</p>
          <p>importe</p>
          <p>iva</p>
          <p>total</p>
          <p>confirmar</p>
        </div>
        <div>
          <form onSubmit={handleSubmit} className='formApunte'>
            <p><input name='fecha' required value={fecha} onChange={setFecha}
              type="date" placeholder="fecha" className='inputFecha'
            /></p>
            <p><select
              className='selecTipo'
              name="tipo"
              value={tipo}
              onChange={setTipo}>
              <option value="" className="TextContainerRow" disabled selected>tipo ingreso</option>
              <option>CAIXA</option>
              <option>CAIXA2</option>
              <option>MAFARI</option>
              <option>DARDOS</option>
              <option>TABACO</option>
              <option>KIROLBET</option>
              <option>OTROS</option>
            </select></p>
            <p><input name='importe' required value={importe} onChange={setImporte}
              type='number' placeholder="importe" className='inputImporte'
            /></p>
            <p><select
              className='selecTipo'
              name="iva"
              type='number'
              value={iva}
              onChange={setIva}>
              <option value="" className="TextContainerRow" disabled selected hidden>tipo iva</option>
              <option>0</option>
              <option>4</option>
              <option>10</option>
              <option>21</option>
            </select></p>

            <p><input name='total' requiredvalue={total} onChange={setTotal}
              type='text' placeholder="total" className='inputImporte'
            /></p>
            <p><input name="crear apunte" type="submit" value="crear apunte" className='inputCrear' /></p>
          </form>
        </div>
        <div className='apunteDescrip'>
          <p>Pulsa en la casilla e introduce la fecha exacta del apunte. Tienes dos opciones,con el teclado númerico añade la fecha con el formato modelo, o despliega el calendario en la flecha laterar y seleciona la fecha</p>
          <p>descripcion</p>
          <p>descripcion</p>
          <p>descripcion</p>
          <p>descripcion</p>
          <p>descripcion</p>
        </div>
      </div>

      <NavLink to="/ingresos" exact activeClassName="active">
        {mensaje} volver
          </NavLink>
    </div>
  )
}

export default Apunte