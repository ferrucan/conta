import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import Menu from '../../menu';



const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}

const Consultas = () => {

  const [fechaIni, setFechaIni] = useFormField()
  const [fechaFin, setFechaFin] = useFormField()
  const [tipo, setTipo] = useFormField()
  const history = useHistory()
  const handleSubmit = async (e) => {
    e.preventDefault()
    history.push(`/rango/${tipo}/${fechaIni}/${fechaFin}`)

  }
  return (

    <div className='divcont'>
      <Menu />
      <div className='apunte' >
        <div className='apunteTitulos'>
          <p>fecha de inicio</p>
          <p>fecha final</p>
          <p>tipo de apunte</p>
          <p>consultar</p>

        </div>
        <form onSubmit={handleSubmit} className='formApunte'>
          <p><input name='fecha' required value={fechaIni} onChange={setFechaIni}
            type="date" placeholder="fecha" className='inputFecha'
          /></p>
          <p><input name='fecha' required value={fechaFin} onChange={setFechaFin}
            type="date" placeholder="fecha" className='inputFecha'
          /></p>
          <p><select
            className='selecTipo'
            name="tipo"
            required value={tipo}
            onChange={setTipo}>
            <option className="TextContainerRow">tipo ingreso</option>
            <option >CAIXA</option>
            <option>CAIXA2</option>
            <option>MAFARI</option>
            <option>DARDOS</option>
            <option>TABACO</option>
            <option>KIROLBET</option>
            <option>OTROS</option>
          </select></p>
          <p>
            <input name="CONSULTAR" type="submit" value="CONSULTAR" className='inputCrear' />
          </p>

        </form>
      </div>



    </div>
  )

}





export default Consultas