import React, { useState, useEffect } from 'react'
import {  useParams } from 'react-router-dom'
import Menu from '../../menu';

const ResultadoConsultas = () => {
   const {fechaIni} = useParams()
    const [total, setTotal] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/rango/')
            .then(r => r.json())
            .then(data => setTotal(data))
    }, [])
    if (!total) return 'loading...'

    return(
    <div className='divcont'>  
          <Menu/>
       <p>total ingresos {total[0]}</p>
       <p>total ingresos sin iva{total[1]}</p>
      
    </div>
    )

}
export default ResultadoConsultas
