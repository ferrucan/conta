import React, { useState, useEffect } from 'react';
import Menu from '../../menu';
const Total = () =>{
    const [total, setTotal] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/ingresos')
            .then(r => r.json())
            .then(data => setTotal(data))
    }, [])
    if (!total) return 'loading...'

    return(
    <div className='divcont'>  
          <Menu/>
       <p>total ingresos {total[0]}</p>
       <p>total sin iva {total[1]}</p>
    </div>
    )

}


export default Total