import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Menu from '../../menu';

const ConsultaFechas = () => {
    const { fechaIni, fechaFin, tipo } = useParams()
    const [total, setTotal] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/rango')
            .then(r => r.json())
            .then(data => setTotal(data))
    }, [])

    if (!total) return 'loading...'

    const entries1 = total
        .filter(i => i.fecha && i.fecha.substring(0, 10) > fechaIni)
    const entries2 = entries1
        .filter(j => j.fecha && j.fecha.substring(0, 10) < fechaFin)
    const entries = entries2
        .filter(k => k.tipo && k.tipo === tipo)
    const tots = entries.map(t => parseInt(t.total))
    const sum = (array) => {
        var acum = 0;
        for (var i = 0; i < array.length; i++) {
            acum = acum + (array[i]);
        }
        return acum;
    }
    const totales = sum(tots)

    if (!entries.length) return 'no existe ninguna oferta en esta fecha'
    return (
        <div className='divcont'>
            <Menu />
            <div className='rConsulta'>
                <div className='rConsultaTotal'>
                    <span>TOTAL ingresos {tipo} = {totales}€</span>
                    <span> desde {fechaIni} a {fechaFin}</span>
                    </div>
                <div >
                    {entries.map(apunte =>

                        <div key={apunte.id} className='unidadConsulta'>
                            <div >
                                <p >{apunte.fecha.substring(0, 10)}</p>
                            </div>
                            <div >
                                <h4>{apunte.total}€</h4>
                            </div>

                        </div>

                    )}
                </div></div>

        </div>
    )

}
export default ConsultaFechas
