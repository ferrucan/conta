import React, { useState, useEffect } from 'react';
import Menu from '../../menu';
import { useParams } from 'react-router-dom'

const Tipo = () => {
    const { tipo } = useParams()
    const [ingreso, setIngreso] = useState()
    useEffect(() => {
        fetch('http://localhost:8080/ingresos')
            .then(r => r.json())
            .then(data => setIngreso(data))
    }, [])
    if (!ingreso) return 'loading...'
    const entries = ingreso.filter(i => i.tipo === tipo)
    if (!entries.length) return 'no existe ninguna oferta en este rango'

    return (
        <div className='divcont'>
            <Menu />
            <div>
                {entries.map(ingreso =>
                    <div>
                        <header>
                            <h1>{ingreso.fecha}</h1>
                        </header>
                        <section>
                            <h2>{ingreso.iva}</h2>
                            <h3>{ingreso.total}</h3>
                        </section>
                    </div>
                )}
            </div>
        </div>
    )
}


export default Tipo