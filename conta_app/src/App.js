import React from 'react';
import {
  BrowserRouter as Router
  
} from "react-router-dom"

import './App.css';

import Contenido from './contenido/contenido'
function App() {
  return (
   <Router>
      <div className="App">
        <Contenido />
      </div>
      </Router>
  );
}

export default App;
